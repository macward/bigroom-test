//
//  LoginViewController.swift
//  BigRoomTest
//
//  Created by Max Ward on 21/02/2022.
//

import UIKit
import SwiftUI

class LoginViewController: UIViewController, NotifyDelegate, ChangeStateDelegate {
    
    // PROPERTIES
    var viewModel: LoginViewModelProtocol?
    var inputSeparator: NSLayoutConstraint!
    var buttonSeparator: NSLayoutConstraint!
    var hasError: Bool = false
    // UI COMPONENTS
    
    let logoImage: UIImageView = {
        let imageView = UIImageView(frame: .zero)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "logo")
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    let emailTextField: PrimaryTextField = {
        let textField = PrimaryTextField(frame: .zero)
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.attributedPlaceholder = NSAttributedString(
            string: "Email",
            attributes: [NSAttributedString.Key.foregroundColor: UIColor(named: "SecondaryContrast") ?? UIColor.white]
        )
        return textField
    }()
    
    let passwordTextField: PrimaryTextField = {
        let textField = PrimaryTextField(frame: .zero)
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.attributedPlaceholder = NSAttributedString(
            string: "Password",
            attributes: [NSAttributedString.Key.foregroundColor: UIColor(named: "SecondaryContrast") ?? UIColor.white]
        )
        textField.isSecureTextEntry = true
        return textField
    }()
    
    let loginButton: PrimaryButton = {
        let btn = PrimaryButton(frame: .zero)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.setTitle("Login", for: .normal)
        return btn
    }()
    
    let emailErrorLabel: UILabel = {
        let lbl = UILabel(frame: .zero)
        lbl.font = UIFont.systemFont(ofSize: 12, weight: .semibold)
        lbl.textColor = UIColor.white
        lbl.text = "Generic email error message"
        return lbl
    }()
    
    let passwordErrorLabel: UILabel = {
        let lbl = UILabel(frame: .zero)
        lbl.font = UIFont.systemFont(ofSize: 12, weight: .semibold)
        lbl.textColor = UIColor.white
        lbl.text = "Generic password error message"
        return lbl
    }()
    
    init(viewModel: LoginViewModelProtocol) {
        super.init(nibName: nil, bundle: nil)
        self.viewModel = viewModel
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        super.loadView()
        
        self.hideKeyboardWhenTappedAround()
        
        view.backgroundColor = UIColor(named: "Background")
        
        view.addSubview(logoImage)
        logoImage.topAnchor.constraint(equalTo: view.topAnchor, constant: 106).isActive = true
        logoImage.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        logoImage.widthAnchor.constraint(equalToConstant: 68).isActive = true
        logoImage.heightAnchor.constraint(equalToConstant: 68.0).isActive = true
        
        
        view.addSubview(emailTextField)
        emailTextField.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16).isActive = true
        emailTextField.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16).isActive = true
        emailTextField.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: -70).isActive = true
        emailTextField.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        view.addSubview(passwordTextField)
        
        passwordTextField.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16).isActive = true
        passwordTextField.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16).isActive = true
        inputSeparator = passwordTextField.topAnchor.constraint(equalTo: emailTextField.bottomAnchor, constant: 12)
        inputSeparator.isActive = true
        passwordTextField.heightAnchor.constraint(equalTo: emailTextField.heightAnchor).isActive = true
        
        view.addSubview(loginButton)
        
        loginButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16).isActive = true
        loginButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16).isActive = true
        buttonSeparator = loginButton.topAnchor.constraint(equalTo: passwordTextField.bottomAnchor, constant: 16)
        buttonSeparator.isActive = true
        loginButton.heightAnchor.constraint(equalTo: emailTextField.heightAnchor).isActive = true
        
        loginButton.addTarget(self, action: #selector(submitButtonAction), for: .touchUpInside)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel?.notifyDelegate = self
        emailTextField.stateDelegate = self
        passwordTextField.stateDelegate = self
    }
    
    @objc func submitButtonAction(_ sender: AnyObject) {
        let email = self.emailTextField.text
        let password = self.passwordTextField.text
        viewModel?.authUser(with: email, password: password)
    }
    
    func notifySuccess() {
        //
    }
    
    func notifyError(error: Error) {
        
        let error = error as! LoginViewModel.ValidationSates
        hasError = true
        switch error {
        case .emptyEmail:
            showEmailError()
        case .wrongEmail:
            showEmailError()
        case .emptyPassword:
            passwordError()
        case .wrongPassword:
            passwordError()
        case .wrongCredentials:
            showEmailError()
            passwordError()
        }
    }
    
    private func showEmailError() {
        view.addSubview(emailErrorLabel)
        emailErrorLabel.frame = CGRect(x: emailTextField.frame.origin.x,
                                       y: emailTextField.frame.maxY + 2,
                                       width: emailTextField.frame.width,
                                       height: 20)
        
        emailTextField.setState(.error)
        inputSeparator.isActive = false
        inputSeparator = passwordTextField.topAnchor.constraint(equalTo: emailTextField.bottomAnchor, constant: 30)
        inputSeparator.isActive = true
        UIView.animate(withDuration: 0.1) {
            self.view.layoutIfNeeded()
        }
    }
    
    private func hideEmailError() {
        emailErrorLabel.removeFromSuperview()
        emailTextField.clear()
        emailTextField.setState(.idle)
        inputSeparator.isActive = false
        inputSeparator = passwordTextField.topAnchor.constraint(equalTo: emailTextField.bottomAnchor, constant: 12)
        inputSeparator.isActive = true
        UIView.animate(withDuration: 0.1) {
            self.view.layoutIfNeeded()
        }
    }
    
    private func passwordError() {
        
        view.addSubview(passwordErrorLabel)
        emailErrorLabel.frame = CGRect(x: passwordTextField.frame.origin.x,
                                       y: passwordTextField.frame.maxY + 2,
                                       width: emailTextField.frame.width,
                                       height: 20)
        
        passwordTextField.setState(.error)
        buttonSeparator.isActive = false
        buttonSeparator = loginButton.topAnchor.constraint(equalTo: passwordTextField.bottomAnchor, constant: 34)
        buttonSeparator.isActive = true
        UIView.animate(withDuration: 0.1) {
            self.view.layoutIfNeeded()
        }
    }
    
    private func hidePasswordError() {
        passwordErrorLabel.removeFromSuperview()
        passwordTextField.clear()
        passwordTextField.setState(.idle)
        buttonSeparator.isActive = false
        buttonSeparator = loginButton.topAnchor.constraint(equalTo: passwordTextField.bottomAnchor, constant: 14)
        buttonSeparator.isActive = true
        UIView.animate(withDuration: 0.1) {
            self.view.layoutIfNeeded()
        }
    }
    
    func isFocused() {
        if hasError == true {
            hideEmailError()
            hidePasswordError()
            hasError = false
        }
    }
}


