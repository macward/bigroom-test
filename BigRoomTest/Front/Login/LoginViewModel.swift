//
//  LoginViewModel.swift
//  BigRoomTest
//
//  Created by Max Ward on 21/02/2022.
//

import Foundation

protocol LoginViewModelProtocol {
    var notifyDelegate: NotifyDelegate? { get set }
    func authUser(with email: String?, password: String?)
    func validateCredentials(_ email: String?, password: String?) -> Bool
}

class LoginViewModel: LoginViewModelProtocol {
    
    var requestDispatcher: RequestDispatcher!
    var notifyDelegate: NotifyDelegate?
    
    enum ValidationSates: Error {
        case emptyEmail
        case wrongEmail
        case emptyPassword
        case wrongPassword
        case wrongCredentials
    }
    
    init(session: NetworkSession) {
        self.requestDispatcher = RequestDispatcher(networkSession: session)
    }
    
    func validateCredentials(_ email: String?, password: String?) -> Bool {
        
        // check if email is empty
        if email == nil || email == "" {
            notifyDelegate?.notifyError(error: ValidationSates.emptyEmail)
        }
        // check if email is well formated
        
        // check is min pass charachters is fine
        
        // chck if pass field is not empty
        
        // pass all checks
        return false
    }
    
    func isValidPassword(_ password: String) -> Bool {
        return true
    }
    
    func authUser(with email: String?, password: String?) {
        
        guard self.validateCredentials(email, password: password) == true else { return }
        
//        requestDispatcher.execute(request: AuthEndpoints.login(email, password), of: AuthDTO.self) { response in
//            switch response {
//            case .success(let value):
//                // do something with success value
//                print(value ?? "")
//            case .failure(let error):
//                // notify ui
//                print(error)
//            }
//        }
    }
    
}
