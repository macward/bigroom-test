//
//  PrimaryButton.swift
//  BigRoomTest
//
//  Created by Max Ward on 21/02/2022.
//

import UIKit
import SwiftUI

class PrimaryButton: UIButton {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        customize()
    }
    
    required init?(coder: NSCoder) {
        fatalError()
    }
    
    private func customize() {
        backgroundColor = UIColor(named: "Primary")
        setTitleColor(UIColor(named: "PrimaryTextContrast"), for: .normal)
        titleLabel?.font = UIFont.systemFont(ofSize: 16, weight: .semibold)
        layer.cornerRadius = 4
        layer.masksToBounds = true
    }
    
}
