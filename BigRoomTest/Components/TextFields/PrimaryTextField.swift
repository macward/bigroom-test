//
//  BorderedTextField.swift
//  BigRoomTest
//
//  Created by Max Ward on 21/02/2022.
//

import UIKit
import SwiftUI

protocol ChangeStateDelegate {
    func isFocused()
}

class PrimaryTextField: UITextField, UITextFieldDelegate {
    
    var stateDelegate: ChangeStateDelegate?
    
    enum State {
        case selected
        case idle
        case error
    }
    
    var currentSate: State = .idle
    
    private let textPadding = UIEdgeInsets(
        top: 10,
        left: 20,
        bottom: 10,
        right: 20
    )

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.customize()
    }
    
    required init?(coder: NSCoder) {
        fatalError("")
    }
    
    private func customize() {
        
        self.delegate = self
        
        self.backgroundColor = UIColor(named: "Secondary")
        self.textColor = UIColor(named: "SecondaryContrast")
        setState(currentSate)
        self.layer.borderWidth = 2
        self.layer.cornerRadius = 4
        
        self.font = UIFont.systemFont(ofSize: 16, weight: .semibold)
        
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        let rect = super.textRect(forBounds: bounds)
        return rect.inset(by: textPadding)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        let rect = super.editingRect(forBounds: bounds)
        return rect.inset(by: textPadding)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        stateDelegate?.isFocused()
        if let text = self.text, !text.isEmpty { return }
        setState(.selected)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let text = self.text, !text.isEmpty { return }
        setState(.idle)
    }
    
    func setState(_ state: State) {
        switch state {
        case .idle:
            UIView.animate(withDuration: 0.1) {
                self.layer.borderColor = UIColor(named: "Secondary")?.cgColor
            }
        case .selected:
            UIView.animate(withDuration: 0.1) {
                self.layer.borderColor = UIColor(named: "Primary")?.cgColor
            }
        case .error:
            UIView.animate(withDuration: 0.1) {
                self.layer.borderColor = UIColor.init(named: "RedAlert")?.cgColor
            }
        }
    }
    
    func clear() {
        self.text = ""
    }
    
}
