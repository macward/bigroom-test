//
//  ViewController.swift
//  BigRoomTest
//
//  Created by Max Ward on 21/02/2022.
//
import Foundation

enum AuthEndpoints: RequestProtocol {
    
    case login(String, String)
    case forgotPassword(String)
    
    var baseUrl: String? {
        return "https://cms.bigroom.tv/"
    }
    
    var path: String {
        switch self {
        case .login:
            return "auth/local"
        case .forgotPassword:
            return "auth/forgot-password"
        }
    }

    var method: RequestMethod {
        return .post
    }

    var headers: ReaquestHeaders? {
        return nil
    }

    var parameters: RequestParameters? {
        switch self {
        case .login(let email, let password):
            return ["identifier": email, "password": password]
        case .forgotPassword(let email):
            return ["email": email]
        }
    }
}
