//
//  AuthDTO.swift
//  BigRoomTest
//
//  Created by Max Ward on 21/02/2022.
//

import Foundation

struct AuthDTO: Codable {
    var message: String
}
