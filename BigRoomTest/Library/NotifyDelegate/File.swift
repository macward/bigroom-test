//
//  File.swift
//  BigRoomTest
//
//  Created by Max Ward on 21/02/2022.
//

import Foundation

protocol NotifyDelegate {
    func notifySuccess()
    func notifyError(error: Error)
}
